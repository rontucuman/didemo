package com.dh.didemo.controller;

import com.dh.didemo.Forecast;
import com.dh.didemo.services.GreetingService;
import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContructorBasedControllerTest {
    private ConstructorBasedController constructorBasedController;

    @Before
    public void setUp() throws Exception {
        GreetingService greetingService = new GreetingServiceImpl();
        Forecast forecast = new Forecast();
        constructorBasedController = new ConstructorBasedController(greetingService, forecast);
    }

    @Test
    public void sayHello() {
        String greeting  = constructorBasedController.sayHello();
        Assert.assertEquals("Hello GreetingServiceImpl", greeting);
    }
}