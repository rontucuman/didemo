package com.dh.didemo.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyBasedControllerTest {

    private PropertyBasedController propertyBasedController;
    @Before
    public void setUp() throws Exception {
        propertyBasedController = new PropertyBasedController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void sayHello() {
        String greeting = propertyBasedController.sayHello();
        Assert.assertEquals("hola", greeting);
    }
}