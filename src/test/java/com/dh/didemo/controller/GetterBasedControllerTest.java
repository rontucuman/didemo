package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetterBasedControllerTest {

    private GetterBasedController getterBasedController;

    @Before
    public void setUp() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void sayHello() {
        String gretting = getterBasedController.sayHello();
        Assert.assertEquals("Hello GreetingServiceImpl", gretting);
    }
}