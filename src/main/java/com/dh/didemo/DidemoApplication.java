package com.dh.didemo;

import com.dh.didemo.controller.ConstructorBasedController;
import com.dh.didemo.controller.GetterBasedController;
import com.dh.didemo.controller.MyController;
import com.dh.didemo.controller.PropertyBasedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:beanconfig.xml")
@ComponentScan(basePackages = {"com.dh.didemo"})
public class DidemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DidemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController");
        controller.hello();

        System.out.println(context.getBean(ConstructorBasedController.class).sayHello());
        System.out.println(context.getBean(GetterBasedController.class).sayHello());
        System.out.println(context.getBean(PropertyBasedController.class).sayHello());
        //System.out.println(context.getBean(Forecast.class));
        System.out.println("user: " + context.getBean(FakeDtaSource.class).getUser());
    }
}
