package com.dh.didemo.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
public class GetterGreetingServiceImpl implements GreetingService {
    public static final String GREETING = "Hello Getter";

    @Override
    @Profile("en")
    public String sayGreeting() {
        return GREETING;
    }
}
