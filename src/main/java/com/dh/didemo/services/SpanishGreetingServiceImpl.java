package com.dh.didemo.services;

import org.springframework.context.annotation.Profile;

public class SpanishGreetingServiceImpl implements GreetingService {
    public final String GREETING = "Spanish";
    @Override
    @Profile("es")
    public String sayGreeting() {
        return GREETING;
    }
}
