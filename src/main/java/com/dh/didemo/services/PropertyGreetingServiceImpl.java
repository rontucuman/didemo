package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class PropertyGreetingServiceImpl implements GreetingService {
    public final String GREETING = "Property";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
