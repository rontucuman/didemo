package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceImpl implements GreetingService {
    public static final String GREETING = "Hello ConstructorGreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
